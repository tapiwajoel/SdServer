package service.dashboard.models;

import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "logs")
public class Log {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;

    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "profile_id")
    private Profile profile;


    @Column(name = "session_id")
    private String sessionId;

    @Column(name = "msisdn")
    private String msisdn;


    @Column(name = "message", columnDefinition="TEXT")
    private String message;

    @Column(name = "level")
    private String level;

    @CreationTimestamp
    @Column(name = "date")
    private Date date;

    public Log() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Profile getProfile() {
        return profile;
    }

    public void setProfile(Profile profile) {
        this.profile = profile;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "Log{" +
                "id=" + id +
                ", profile=" + profile +
                ", sessionId='" + sessionId + '\'' +
                ", msisdn='" + msisdn + '\'' +
                ", message='" + message + '\'' +
                ", level='" + level + '\'' +
                ", date=" + date +
                '}';
    }
}
